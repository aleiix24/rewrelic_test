## New Relic Apdex Board - Code challenge by aleix casanovas

## Start

```Shell
yarn run start
```

Starts the server running on `http://localhost:3000` with the already compiled code

### Compile code

```Shell
yarn run buildServer
yarn run buildWeb
```

### Tests
```Shell
yarn run tests
```