"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _toArray(arr) { return Array.isArray(arr) ? arr : Array.from(arr); }

/**
 * Quicksort implementation
 * 
 * Best case: O(n log n).
 * Average case: O(n log n).
 * Worst case: O(n^2).
 * 
 */
var sortByApdex = exports.sortByApdex = function sortByApdex(array) {
  if (array.length === 0) return [];

  var _array = _toArray(array),
      x = _array[0],
      xs = _array.slice(1);

  return [].concat(_toConsumableArray(sortByApdex(xs.filter(function (y) {
    return y.getApdex() >= x.getApdex();
  }))), [x], _toConsumableArray(sortByApdex(xs.filter(function (y) {
    return y.getApdex() < x.getApdex();
  }))));
};