'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _sortByApdex = require('./sortByApdex');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var RESPONSE_LIMIT = 25;

var appsService = function () {
  function appsService(apps) {
    _classCallCheck(this, appsService);

    this.applicationsList = (0, _sortByApdex.sortByApdex)(apps);
  }

  _createClass(appsService, [{
    key: 'getTopAppsByHost',
    value: function getTopAppsByHost(targetHost) {
      var _this = this;

      return new Promise(function (resolve, reject) {
        resolve(_this.applicationsList.filter(function (app) {
          return app.getHost().includes(targetHost);
        }).slice(0, RESPONSE_LIMIT));
      });
    }
  }, {
    key: 'addAppToHost',
    value: function addAppToHost(appName, host) {
      this.applicationsList = this.applicationsList.map(function (app) {
        if (app.getName() === appName) {
          app.addHost(host);
        }
        return app;
      });
    }
  }, {
    key: 'removeAppFromHost',
    value: function removeAppFromHost(appName, host) {
      this.applicationsList = this.applicationsList.map(function (app) {
        if (app.getName() === appName) {
          app.removeHost(host);
        }
        return app;
      });
    }
  }]);

  return appsService;
}();

exports.default = appsService;