'use strict';

var _index = require('../index');

var _index2 = _interopRequireDefault(_index);

var _Application = require('../../../models/Application');

var _Application2 = _interopRequireDefault(_Application);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var mock = [new _Application2.default({
  name: 'name 1',
  apdex: 1,
  host: ['host_1', 'host_2']
}), new _Application2.default({
  name: 'name 2',
  apdex: 50,
  host: ['host_1']
}), new _Application2.default({
  name: 'name 3',
  apdex: 10,
  host: ['host_3']
}), new _Application2.default({
  name: 'name 4',
  apdex: 90,
  host: ['host_1', 'host_2', 'host_3']
})];

var service = void 0;
beforeEach(function () {
  service = new _index2.default(mock);
});

describe('appsService', function () {
  it('should filter', function () {
    var topApps = service.getTopAppsByHost('host_2');
    expect(topApps).resolves.toHaveLength(2);
  });
  it('should order', function () {
    var topApps = service.getTopAppsByHost('host_2').then(function (topApps) {
      return expect(topApps[0].getApdex()).toBeGreaterThan(topApps[1].getApdex());
    });
  });
  it('should add', function () {
    service.addAppToHost('name 3', 'host_2');
    var topApps = service.getTopAppsByHost('host_2');
    expect(topApps).resolves.toHaveLength(3);
  });
  it('should remove', function () {
    service.removeAppFromHost('name 2', 'host_1');
    var topApps = service.getTopAppsByHost('host_1');
    expect(topApps).resolves.toHaveLength(2);
  });
});