"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Application = function () {
    function Application(app) {
        _classCallCheck(this, Application);

        app && Object.assign(this, app);
    }

    _createClass(Application, [{
        key: "getName",
        value: function getName() {
            return this.name;
        }
    }, {
        key: "getContributors",
        value: function getContributors() {
            return this.contributors;
        }
    }, {
        key: "getVersion",
        value: function getVersion() {
            return this.version;
        }
    }, {
        key: "getApdex",
        value: function getApdex() {
            return this.apdex;
        }
    }, {
        key: "getHost",
        value: function getHost() {
            return this.host;
        }
    }, {
        key: "addHost",
        value: function addHost(host) {
            this.getHost().push(host);
        }
    }, {
        key: "removeHost",
        value: function removeHost(host) {
            var hostIndex = this.getHost().indexOf(host);
            if (hostIndex > -1) {
                this.getHost().splice(hostIndex, 1);
            }
        }
    }]);

    return Application;
}();

exports.default = Application;