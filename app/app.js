import 'whatwg-fetch';
import 'styles.css';

const createAppRow = app => (
  `<li class="row">
    <span class="apdex">${app.apdex}</span>
    <span class="name">${app.name} - ${app.contributors.join(', ')}</span>
    <span class="tooltip">Version: ${app.version}</span>
  </li>`
);

const createTemplateSection = (host, apps) => (
  `<section>
    <header>
      <h2>${host}</h2>
    </header>
    <div class="content">
      <ul>
        ${apps.slice(0,5).map(app => createAppRow(app)).join('')}
      </ul>
    </div>
  </section>`
);

export function addEventListenerChangeListType() {
  document.querySelector('#cbox').addEventListener('change', (event) => {
    document.querySelector('#app').classList.toggle('list');
  })
}

export function addEventListenerTooltips() {
  function findUpClass(el, selector) {
    if (el.matches(selector)) {
        return el;
    }
    while (el.parentNode) {
        el = el.parentNode;
        if (el.matches && el.matches(selector)) {
            return el;
        }
    }
    return null;
  }
  document.addEventListener('click', function (event) {
    var row = findUpClass(event.target, '.row');
    if (row) {
      row.classList.toggle('show-tooltip');
    }
  });
}

export default class App {
  constructor(hosts) {
    hosts.map(host =>
      fetch(`/api/apps/${host}`)
        .then(resp => resp.json())
        .then(data => {
          this.renderHost(host, data.response);
          return data.response;
        })
    );
  }

  renderHost(host, apps) {
    var node = document.querySelector('#app');
    if (!node) return;
    node.innerHTML += createTemplateSection(host, apps);
    console.log(apps);
  }
}

const hosts = [
  '7e6272f7-098e.dakota.biz',
  '9a450527-cdd9.kareem.info',
  'e7bf58af-f0be.dallas.biz',
  'b0b655c5-928a.nadia.biz'
];

new App(hosts);

document.addEventListener("DOMContentLoaded", () => {
  addEventListenerChangeListType();
  addEventListenerTooltips();
});
