class Application {
    constructor(app) {
        app && Object.assign(this, app);
    }

    getName() {
        return this.name;
    }
    getContributors() {
        return this.contributors;
    }
    getVersion() {
        return this.version;
    }
    getApdex() {
        return this.apdex;
    }
    getHost() {
        return this.host;
    }
    addHost(host) {
        this.getHost().push(host);
    }
    removeHost(host) {
        const hostIndex = this.getHost().indexOf(host);
        if (hostIndex > -1) {
            this.getHost().splice(hostIndex, 1);
        }
    }
}

export default Application;