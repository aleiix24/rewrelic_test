
/**
 * Quicksort implementation
 * 
 * Best case: O(n log n).
 * Average case: O(n log n).
 * Worst case: O(n^2).
 * 
 */
export const sortByApdex = array => {
  if (array.length === 0) return [];
  let [x, ...xs] = array;    
  return [
      ...sortByApdex(xs.filter(y => y.getApdex() >= x.getApdex())),
      x,
      ...sortByApdex(xs.filter(y => y.getApdex() < x.getApdex()))
  ];
};