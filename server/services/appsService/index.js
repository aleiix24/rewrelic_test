import { sortByApdex } from './sortByApdex';

const RESPONSE_LIMIT = 25;

class appsService {
  constructor(apps) {
    this.applicationsList = sortByApdex(apps);
  }

  getTopAppsByHost(targetHost) {
    return new Promise((resolve, reject) => {
      resolve(
        this.applicationsList
          .filter(app => app.getHost().includes(targetHost))
          .slice(0, RESPONSE_LIMIT)
      );
    });
  }

  addAppToHost(appName, host) {
    this.applicationsList = this.applicationsList
      .map(app => {
        if (app.getName() === appName) {
          app.addHost(host);
        }
        return app;
      });
  }

  removeAppFromHost(appName, host) {
    this.applicationsList = this.applicationsList
      .map(app => {
        if (app.getName() === appName) {
          app.removeHost(host);
        }
        return app;
      });
  }
}

export default appsService;