import appsService from '../index';
import Application from '../../../models/Application';

const mock = [
  new Application({
    name: 'name 1',
    apdex: 1,
    host: ['host_1', 'host_2']
  }),
  new Application({
    name: 'name 2',
    apdex: 50,
    host: ['host_1']
  }),
  new Application({
    name: 'name 3',
    apdex: 10,
    host: ['host_3']
  }),
  new Application({
    name: 'name 4',
    apdex: 90,
    host: ['host_1', 'host_2', 'host_3']
  }),
];

let service;
beforeEach(() => {
  service = new appsService(mock);
});

describe('appsService', () => {
  it('should filter', () => {
    const topApps = service.getTopAppsByHost('host_2');
    expect(topApps).resolves.toHaveLength(2);
  });
  it('should order', () => {
    const topApps = service.getTopAppsByHost('host_2').then((topApps =>
      expect(topApps[0].getApdex()).toBeGreaterThan(topApps[1].getApdex())
    ));
  });
  it('should add', () => {
    service.addAppToHost('name 3', 'host_2');
    const topApps = service.getTopAppsByHost('host_2');
    expect(topApps).resolves.toHaveLength(3);
  });
  it('should remove', () => {
    service.removeAppFromHost('name 2', 'host_1');
    const topApps = service.getTopAppsByHost('host_1');
    expect(topApps).resolves.toHaveLength(2);
  });
})