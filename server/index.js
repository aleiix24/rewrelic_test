import express from 'express';
import appsService from './services/appsService';
import Application from './models/Application';
import appsArray from './data/host-app-data.json';

const app = express();
const _appsService = new appsService(appsArray.map(app => new Application(app)));

// Backend, API here
app.get('/api/apps/:host', (req, res) =>
  _appsService
    .getTopAppsByHost(req.params.host)
    .then(response => res.send({ response: response })),
);

// WebApp here
app.use(express.static('build'));

const port = process.env.PORT || 3000;

app.listen(port, () => {
  console.log(`Open http://localhost:${port} in your browser`);
});